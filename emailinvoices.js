function emailInvoices(){
	nlapiLogExecution('debug', 'start');
	try{
		var invoiceSearch = searchInvoices();
		if(!invoiceSearch || invoiceSearch.length < 1){
			nlapiLogExecution('debug', 'No transactions found');
			return;
		}
		nlapiLogExecution('debug', 'invoiceSearch length', invoiceSearch.length);
		for(var i = 0; i < invoiceSearch.length; i++){
			nlapiLogExecution('debug', 'Processing inv:',invoiceSearch[i].getId());
			var invRec = nlapiLoadRecord('invoice', invoiceSearch[i].getId());
			var customerRec = nlapiLoadRecord('customer', invRec.getFieldValue('entity'));
			var author = 28; // Stacey Chang
			var firstEmail = customerRec.getFieldValue('email');
			var secondEmail = customerRec.getFieldValue('custentity1');
			var thirdEmail = customerRec.getFieldValue('custentity5');
			var recipient = firstEmail;
			var cc = null;
			if(secondEmail && secondEmail != firstEmail){
				cc += secondEmail + ', ';
			}
			if(thirdEmail && secondEmail != thirdEmail){
				cc += thirdEmail;
			}
			var records = invRec;
			var attachments = invRec;
			var subject = 'Fonteva Inc.: Invoice #' + invRec.getFieldValue('tranid');
			var body = 'Dear Customer, <br/><br/>';
			body += 'Please find attached your invoice with Fonteva. If you have any questions, please contact Accounts Receivable at AR@fonteva.com. <br/><br/>';
			body += 'Sincerely,<br/><br/>';
			body += 'Stacey Chang<br/>'
			body += 'Accounts Receivable <br/>';

			nlapiLogExecution('debug', 'attachments', attachments);
			nlapiLogExecution('debug','author', author );
			nlapiLogExecution('debug', 'recipient', recipient);
			nlapiLogExecution('debug', 'subject', subject);
			nlapiLogExecution('debug','cc', cc );
			nlapiLogExecution('debug', 'records', records);
			try{
				var attach = nlapiPrintRecord('TRANSACTION', invRec.getFieldValue('id'));
				nlapiLogExecution('debug', 'step 1', invRec.getFieldValue('id'));
				nlapiSendEmail(author, recipient, subject, body, cc, null, null, attach);
				nlapiLogExecution('debug', 'Emails sent');
				invRec.setFieldValue('custbody_email_sent', 'T');
				nlapiSubmitRecord(invRec);
				nlapiLogExecution('debug', 'Record submitted');
			}catch(exc){
				nlapiLogExecution('error', 'Error', exc.message)
			}
		}
		nlapiLogExecution('debug', 'Finish');
	}catch(ex){
		nlapiLogExecution('error', 'Error: ', ex.message);
	}
}

function searchInvoices(){
	var searchID = 'customsearch_invoices_pending_email_2';
	var results = nlapiSearchRecord('invoice', searchID);
	return results;
}