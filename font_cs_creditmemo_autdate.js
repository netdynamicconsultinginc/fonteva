/**
 * @NApiVersion 2.0
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 */
define(['N/record','N/format'],

function(record,format) {
 
    /**
     * Validation function to be executed when record is saved.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @returns {boolean} Return true if record is valid
     *
     * @since 2015.2
     */
    function saveRecord(context) {
		var creditmemo 	= context.currentRecord;
    	
    	var tranDate 			= creditmemo.getText({fieldId: 'trandate'});
		var salesEffectiveDate 	= creditmemo.getText({fieldId: 'saleseffectivedate'});
		var createdDate 		= creditmemo.getText({fieldId: 'custbody_esc_created_date'});
		var lastModifiedDate 	= creditmemo.getText({fieldId: 'custbody_esc_last_modified_date'});
		
		//var startDate 	= creditmemo.getText({fieldId: 'custcol_swe_contract_start_date'});
    	//var endDate 	= creditmemo.getText({fieldId: 'custcol_swe_contract_end_date'});
		
   
		var subCount =creditmemo.getLineCount({ sublistId : 'item'});
      	
		if(!tranDate){
			
			alert("Transaction Date Cannot Be Empty");
			return false;
			
		}else{

			var tranDateObject = format.parse({ value: tranDate, type: format.Type.DATE});
			//var startDateObject = format.parse({ value: startDate, type: format.Type.DATE});
			//var endDateObject 	= format.parse({ value: endDate, type: format.Type.DATE});
			//var salesEffectiveDateObject = format.parse({ value: saleseffectivedate, type: format.Type.DATE});
			//var createdDateObject 	= format.parse({ value: custbody_esc_created_date, type: format.Type.DATE});
			//var lastModifiedDateObject = format.parse({ value: custbody_esc_last_modified_date, type: format.Type.DATE});
			
			creditmemo.setValue({fieldId:'custbody_swe_contract_start_date',value:tranDateObject});
			creditmemo.setValue({fieldId:'custbody_swe_contract_end_date',value:tranDateObject});
			creditmemo.setValue({fieldId:'saleseffectivedate',value:tranDateObject});
			//creditmemo.setValue({fieldId:'custbody_esc_created_date',value:tranDateObject});
			//creditmemo.setValue({fieldId:'custbody_esc_last_modified_date',value:tranDateObject});
           
			var i = 0;
			
    		for(i; i < subCount; i++){
				
				var lineNum = creditmemo.selectLine({sublistId: 'item',line: i});

    			creditmemo.setCurrentSublistValue({ sublistId : 'item',fieldId   : 'custcol_swe_contract_start_date',value : tranDateObject});
				
    			creditmemo.setCurrentSublistValue({ sublistId : 'item',fieldId   : 'custcol_swe_contract_end_date',value   : tranDateObject});
				
    		}
          	creditmemo.commitLine('item');
				
			
		}
		return true;
    }

    return {
        saveRecord: saveRecord
    };
    
});